-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para carreras
CREATE DATABASE IF NOT EXISTS `carreras` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `carreras`;

-- Volcando estructura para tabla carreras.carreras
DROP TABLE IF EXISTS `carreras`;
CREATE TABLE IF NOT EXISTS `carreras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codcar` char(4) NOT NULL COMMENT 'codigo carrera',
  `plan` char(8) NOT NULL COMMENT 'numero resolucion plan',
  `nombre` varchar(160) NOT NULL,
  `activa` enum('No','Si') DEFAULT 'Si' COMMENT 'activa para sistema',
  PRIMARY KEY (`id`),
  UNIQUE KEY `codcar` (`codcar`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='Carreras del instituto';

-- Volcando datos para la tabla carreras.carreras: ~4 rows (aproximadamente)
DELETE FROM `carreras`;
/*!40000 ALTER TABLE `carreras` DISABLE KEYS */;
INSERT INTO `carreras` (`id`, `codcar`, `plan`, `nombre`, `activa`) VALUES
	(1, '1201', '00931-95', 'SEGURIDAD, HIGIENE Y CONTROL AMBIENTAL INDUSTRIAL', 'No'),
	(2, '1202', '00273-03', 'TECNICATURA SUPERIOR EN ADMINISTRACION CONTABLE', 'Si'),
	(3, '1203', '00277-03', 'TECNICATURA SUPERIOR EN ADMINISTRACION FINANCIERA', 'No'),
	(4, '1204', '05817-03', 'TECNICATURA SUPERIOR EN ANALISIS DE SISTEMAS', 'No');
/*!40000 ALTER TABLE `carreras` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;

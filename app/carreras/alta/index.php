<!doctype html>
<html lang="es">

<head>
    <title>Nueva Carrera</title>
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
   
</head>

<body>
    <div class="container-fluid">
        <header>
            <!--navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"> Sistema de Administración </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a id="menuCarreras" class="nav-link dropdown-toggle" href="../../app/listado" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Carreras
                            </a>
                            <div class="dropdown-menu" aria-labelledby="menuCarreras">
                                <a class="dropdown-item" href="../alta">Nuevo</a>
                                <a class="dropdown-item" href="../listado">Edición</a>
                            </div>
                        </li>
                    </ul>
                    <span class="navbar-text">
                        Hola Cosme Fulanito! &nbsp;
                    </span>
                    <button type="button" class="btn btn-sm btn-outline-light">Cerrar Sesion</button>
                </div>
            </nav><!-- ./navbar -->
            <hr>
            <h1 class="mx-3"> Alta Nueva Carrera </h1>
            <hr>
        </header>
        
        <!-- Formulario -->
        <section class="mx-3">
            <form name="formCarrera" action="guardar.php" method="POST">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="codcar">Codigo de Carrera:</label>
                        <input class="form-control" type="text" name="codcar" placeholder="Ej:1201">
                    </div><!-- ./col -->
                    <div class="form-group col-md-6">
                        <label for="plan"> Número de Resolución:</label>
                        <input class="form-control" type="text" name="plan">
                    </div><!-- ./col -->
                </div><!-- ./row -->
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="nombre">Nombre Completo:</label>
                        <input class="form-control" type="text" name="nombre">
                    </div><!-- ./col -->
                </div><!-- ./row -->
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="activa">¿Se encuentra Activa actualmente?:</label>
                        <div>
                            <div class="form-group form-check-inline">
                                <input class="form-check-input" type="radio" name="activa" value="Si"><span class="form-check-label"> Si </span>
                            </div>
                            <div class="form-group form-check-inline">
                                <input class="form-check-input" type="radio" name="activa" value="No"><span class="form-check-label"> No </span>
                            </div>
                        </div>
                    </div><!-- ./col -->
                </div><!-- ./row -->

                <div class="d-flex justify-content-center">
                    <div class="col-md-2">
                        <input class="btn btn-primary btn-lg btn-block" type="submit" value="ENVIAR">
                    </div><!-- ./col -->
                    <div class="col-md-2">
                        <input class="btn btn-secondary btn-lg btn-block" type="button" value="CANCELAR">
                    </div><!-- ./col -->
                </div><!-- ./row -->
            </form>
        </section>
    </div><!-- ./container-fluid -->
   
    <!-- Optional JavaScript -->
    <!-- JQuery first, then Popper.js, the Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
   
</body>

</html>
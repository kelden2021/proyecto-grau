<?php
include "../../funciones.php";

$i    = 0;
$link = conexion();
$sql  = "SELECT * FROM carreras";
$res  = mysqli_query($link, $sql) or die(mysqli_error($link));
mysqli_close($link);

?>
<!doctype html>
<html lang="es">

<head>
    <title>Formulario</title>
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- DataTables CSS -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

    <!-- Sweet Alert -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.css">
</head>

<body>
    <div class="container-fluid">
        <header>
            <!--navbar-->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"> Sistema de Administración </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a id="menuCarreras" class="nav-link dropdown-toggle" href="../../app/listado" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Carreras
                            </a>
                            <div class="dropdown-menu" aria-labelledby="menuCarreras">
                                <a class="dropdown-item" href="../alta">Nuevo</a>
                                <a class="dropdown-item" href="../listado">Edición</a>
                            </div>
                        </li>
                    </ul>
                    <span class="navbar-text">
                        Hola Cosme Fulanito! &nbsp;
                    </span>
                    <button type="button" class="btn btn-sm btn-outline-light">Cerrar Sesion</button>
                </div>
            </nav><!-- ./navbar -->
            <hr>
            <h1 class="mx-3"> Editar o Eliminar Carreras </h1>
            <hr>
        </header>
        <section class="mx-3">
            <table id="carreras">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>CODIGO</th>
                        <th>NOMBRE</th>
                        <th>RESOLUCION</th>
                        <th>ACTIVA</th>
                        <th>EDITAR</th>
                        <th>BORRAR</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>CODIGO</th>
                        <th>NOMBRE</th>
                        <th>RESOLUCION</th>
                        <th>ACTIVA</th>
                        <th>EDITAR</th>
                        <th>BORRAR</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    while ($row = mysqli_fetch_assoc($res)) { //Guardar datos en $row como array asociativo.
                        $i++; //Incrementar el contador
                        echo
                        "<tr>
					        <td> $i </td>
					        <td>{$row['codcar']}</td>
                            <td>{$row['nombre']}</td>
					        <td>{$row['plan']}</td>
                            <td>{$row['activa']}</td>
                            <td><a class=\"btn btn-outline-dark\" role=\"button\" href=\"../editar/index.php?id={$row['id']}\"><i class=\"fas fa-pencil-alt\"></i></a></td>
                            <td><button class=\"btn btn-outline-danger\" onclick=\"confirmaBorrar('{$row['id']}','{$row['plan']}')\"><i class=\"fas fa-trash-alt\"></i></button></td>
				        </tr>";
                    }
                    ?>
                <tbody>

            </table>
        </section>

    </div><!-- ./container-fluid -->

    <!-- Optional JavaScript -->
    <!-- JQuery first, then Popper.js, the Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <!-- DataTables JS -->
    <script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

    <!-- Sweet Alerts -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.all.min.js"></script>

    <script>
        //Sweet Alert Confirmation box
        function confirmaBorrar(id, plan) {
            Swal.fire({
                title: '¿Eliminar Carrera Res. Nro ' + plan + '?',
                text: "Esta acción es irreversible!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.replace('../borrar/borrar.php?id=' + id);
                }
            })
        }

        $(document).ready(function() {
            //Inicializar DataTables            
            $('#carreras').DataTable();

        });
    </script>

</body>

</html>